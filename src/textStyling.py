from gi.repository import Gtk


class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="UList")
        self.set_border_width(20)
        self.set_default_size(500, 300)

        # Boxes
        hbox = Gtk.Box(spacing=20)
        hbox.set_homogeneous(False)
        vbox_left = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=20)
        vbox_left.set_homogeneous(False)
        vbox_right = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=20)
        vbox_right.set_homogeneous(False)

        # Pack the two columns
        hbox.pack_start(vbox_left, True, True, 0)
        hbox.pack_start(vbox_right, True, True, 0)

        label = Gtk.Label("ListU - List Utility Tool")
        vbox_left.pack_start(label, True, True, 0)

        # left align
        label = Gtk.Label()
        label.set_text("Left Aligned Text. \nNew Line entered. ")
        label.set_justify(Gtk.Justification.LEFT)
        vbox_left.pack_start(label, True, True, 0)

        # right align
        label = Gtk.Label()
        label.set_text("Right Aligned Text. \nNew Line entered. ")
        label.set_justify(Gtk.Justification.RIGHT)
        vbox_right.pack_start(label, True, True, 0)

        # markup
        label = Gtk.Label()
        label.set_markup("<small>Small text</small>\n"
                         "<big>Small text</big>\n"
                         "<b>Small text</b>\n"
                         "<i>Small text</i>\n"
                         "<a href=\"https://bitbucket.org/tebeling/python_gtk/src/main/\" title=\"Hover Text\" >Repo</a>")

        label.set_line_wrap(True)
        vbox_right.pack_start(label, True, True, 0)

        self.add(hbox)


window = MainWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()
