from gi.repository import Gtk


class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="UList")

        # Box
        self.box = Gtk.Box(spacing=10)
        self.add(self.box)

        # Button
        self.process_button = Gtk.Button(label="Process")
        self.process_button.connect("clicked", self.process_btn_clicked)
        self.box.pack_start(self.process_button, True, True, 0)

        self.process_button = Gtk.Button(label="Copy")
        self.process_button.connect("clicked", self.copy_btn_clicked)
        self.box.pack_start(self.process_button, True, True, 0)

    def process_btn_clicked(self, widget):
        print("process button clicked")

    def copy_btn_clicked(self, widget):
        print("copy button clicked")


window = MainWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()
