from gi.repository import Gtk


class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="UList")
        self.set_border_width(10)
        listbox = Gtk.ListBox()
        listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        self.add(listbox)

        # Checkbox
        row_1 = Gtk.ListBoxRow()
        box_1 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=100)
        row_1.add(box_1)
        label = Gtk.Label("Invert Sort            ")
        check = Gtk.CheckButton()
        box_1.pack_start(label, True, True, 0)
        box_1.pack_start(check, True, True, 0)
        listbox.add(row_1)

        row_3 = Gtk.ListBoxRow()
        box_3 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=100)
        row_3.add(box_3)
        label = Gtk.Label("Ignore Case           ")
        check = Gtk.CheckButton()
        box_3.pack_start(label, True, True, 0)
        box_3.pack_start(check, True, True, 0)
        listbox.add(row_3)

        row_4 = Gtk.ListBoxRow()
        box_4 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=100)
        row_4.add(box_4)
        label = Gtk.Label("Remove Duplicates")
        check = Gtk.CheckButton()
        box_4.pack_start(label, True, True, 0)
        box_4.pack_start(check, True, True, 0)
        listbox.add(row_4)

        # Toggle
        row_2 = Gtk.ListBoxRow()
        box_2 = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=100)
        row_2.add(box_2)
        label = Gtk.Label("Remove empty lines")
        switch = Gtk.Switch()
        box_2.pack_start(label, True, True, 0)
        box_2.pack_start(switch, True, True, 0)
        listbox.add(row_2)


window = MainWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()
