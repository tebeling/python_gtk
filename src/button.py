from gi.repository import Gtk


class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="UList")

        # Process Button
        self.button = Gtk.Button(label="Process")
        self.button.connect("clicked", self.button_clicked_process)
        self.add(self.button)

    def button_clicked_process(self, widget):
        print("Process Clicked")


window = MainWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()
