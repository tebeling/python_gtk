from gi.repository import Gtk, Gio


class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="ListU ")
        self.set_border_width(10)
        self.notebook = Gtk.Notebook()
        self.add(self.notebook)

        # First Page
        self.page1 = Gtk.Box()
        self.set_border_width(10)
        self.page1.add(Gtk.Label("Main Area 1"))
        self.notebook.append_page(self.page1, Gtk.Label("First Tab"))

        # Second Page
        self.page2 = Gtk.Box()
        self.set_border_width(10)
        self.page2.add(Gtk.Label("Main Area 2"))

        icon = Gtk.Image.new_from_icon_name("system-search-symbolic", Gtk.IconSize.MENU)
        self.notebook.append_page(self.page2, icon)


window = MainWindow()
window.connect("delete-event", Gtk.main_quit)
window.show_all()
Gtk.main()
